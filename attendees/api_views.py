from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods

from events.models import Conference
from .models import Attendee
import json



class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name"
    ]

class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",

    ]
    def get_extra_data(self, o):
        return {"conference": o.conference.name}

@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method =="GET":
        attendees = Attendee.objects.filter(conference = conference_id)


        return JsonResponse(
                    {"attendees": attendees},
                    encoder=AttendeeListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:

            conference  = Conference.objects.get(id=conference_id )
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse({"message": "Invalid conference  id"}, status=400,)


        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
    )
      # attendees_list = [
    #     {
    #             "name": attendee.name,
    #             "href": attendee.get_api_url(),
    #     }
    #         for attendee in attendees
    # ]
    # response = {"attendee": attendees_list}

    # return JsonResponse(response)

@require_http_methods(["DELETE","GET","PUT"])
def api_show_attendee(request, id):

    # attendee = Attendee.objects.get(id=id)
    # return JsonResponse({
    #     "email": attendee.email,
    #     "name": attendee.name,
    #     "company_name": attendee.company_name,
    #     "created": attendee.created,
    #     "conference": {
    #         "name": attendee.conference.name,
    #         "href": attendee.conference.get_api_url(),
    #     }

    # })
    if request.method =="GET":
        attendee = Attendee.objects.get(id=id)

        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse ({"deleted": count > 0})


    else:
        # copied from create
        content = json.loads(request.body)
        try:
            # new code
            if "conference" in content:
                conference = Conference.objects.get(id=content["conference"])
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference Id"},
                status=400,
            )

    # new code
    Attendee.objects.filter(id=id).update(**content)

    # copied from get detail
    attendee = Attendee.objects.get(id=id)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )
