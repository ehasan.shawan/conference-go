from django.http import JsonResponse
from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json





class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "state"
    ]

    # encoders = {
    #         "state": StateEncoder(),
    #     }

    def get_extra_data(self, o):
        return {"state": o.state.name}
class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties =[
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "state",

    ]
    def get_extra_data(self, o):
        return {"state": o.state.name}

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties =[
                "name",
                # "href",
    ]

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "starts",
        "ends",
        "description",
        "created",
        "updated",
        "max_presentations",
        "max_attendees",
    ]
    def get_extra_data(self, o):
        return {"location": o.location.name}





@require_http_methods(["GET", "POST"])
def api_list_conferences(request):

    if request.method =="GET":
        conferences = Conference.objects.all()
    # for conference in conferences:
    #     response.append(
    #         {
    #             "name": conference.name,
    #             "href": conference.get_api_url(),
    #         }

        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:

            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except State.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400,)


        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE","GET","PUT"])
def api_show_conference(request, id):
        # return JsonResponse(
    #     {
    #         "name": conference.name,
    #         "starts": conference.starts,
    #         "ends": conference.ends,
    #         "description": conference.description,
    #         "created": conference.created,
    #         "updated": conference.updated,
    #         "max_presentations": conference.max_presentations,
    #         "max_attendees": conference.max_attendees,
    #         "location": {
    #             "name": conference.location.name,
    #             "href": conference.location.get_api_url(),
    #         },
    #     }
    # )


    if request.method =="GET":
        conference = Conference.objects.get(id=id)

        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse ({"deleted": count > 0})

    else:
        # copied from create
        content = json.loads(request.body)
        try:
            # new code
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location Id"},
                status=400,
            )

    # new code
    Conference.objects.filter(id=id).update(**content)

    # copied from get detail
    conference = Conference.objects.get(id=id)
    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False,
    )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    # """
    # Lists the location names and the link to the location.

    # Returns a dictionary with a single key "locations" which
    # is a list of location names and URLS. Each entry in the list
    # is a dictionary that contains the name of the location and
    # the link to the location's information.
    if request.method == "GET":
        locations = Location.objects.all()

    # locations_list = [
    #         {
    #             "name": location.name,
    #             "href": location.get_api_url(),
    #         }
    #         for location in locations
    #     ]
        return JsonResponse({"locations": locations}, encoder=LocationListEncoder)
    else:

        content = json.loads(request.body)
        try:

            state = State.objects.get(name=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse({"message": "Invalid state name"}, status=400,)



        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )



@require_http_methods(["DELETE","GET","PUT"])
def api_show_location(request, id):
    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
    # location = Location.objects.get(id = id)
    # return JsonResponse({
    #     "name": location.name,
    #     "city": location.city,
    #     "room_count": location.room_count,
    #     "created":location.created,
    #     "updated":location.updated,
    #     "state":{
    #         location.state.name
    #     }
    # })
    if request.method == "GET":
        location = Location.objects.get(id=id)


        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse ({"deleted": count > 0})

    else:
        # copied from create
        content = json.loads(request.body)
        try:
            # new code
            if "state" in content:
                state = State.objects.get(name=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state name"},
                status=400,
            )

    # new code
    Location.objects.filter(id=id).update(**content)

    # copied from get detail
    location = Location.objects.get(id=id)
    return JsonResponse(
        location,
        encoder=LocationDetailEncoder,
        safe=False,
    )
