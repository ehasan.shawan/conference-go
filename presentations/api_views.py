from django.http import JsonResponse

from .models import Presentation, Status
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from events.models import Conference

class StatusEncoder(ModelEncoder):
    model = Status
    properties = ["name"]

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
        "status"
]

    def get_extra_data(self, o):
        return {"status": o.status.name}

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference"
    ]

    def get_extra_data(self, o):
        return {"conference": o.conference.name}

@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):

    # presentations = [
    #     {
    #         "title": p.title,
    #         "status": p.status.name,
    #         "href": p.get_api_url(),
    #     }
    #     for p in Presentation.objects.filter(conference=conference_id)
    # ]
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            presentations,
            encoder=PresentationListEncoder,
            safe=False
        )
    else:
            content = json.loads(request.body)
            try:
                conference = Conference.objects.get(name=content["conference"])
                content["conference"] = conference
            except Conference.DoesNotExist:
                return JsonResponse(
                        {"message": "Invalid conference"},
                        status=400,
                    )
            presentation = Presentation.create(**content)
            return JsonResponse(
                presentation,
                encoder=PresentationDetailEncoder,
                safe=False
            )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):

    # return JsonResponse({
    #     "presenter_name":presenter.presenter_name,
    #     "company_name": presenter.company_name,
    #     "presenter_email":presenter.presenter_email,
    #     "title": presenter.title,
    #     "synopsis": presenter.synopsis,
    #     "created": presenter.created,
    #     "status": presenter.status.name,
    #     "conference":{
    #         "name": presenter.conference.name,
    #         "href": presenter.conference.get_api_url(),
    #     }

    # })
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.methhod == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
